<?php

/**
 * Description of ProduitVue
 *
 * @author ishish
 */
class AuthentificationVue {

    private $parametre;
    private $tpl;
    private $valeurs;

    function __construct($parametre) {
        $this->parametre = $parametre;
        $this->tpl = new Smarty();
    }

    public function chargementValeurs() {

        $this->tpl->assign('titre', 'Gestion La Petite Mousse');

        $this->tpl->assign('titreGestion', 'Authentification');

        $this->tpl->assign('message', AuthentificationTable::getMessageErreur());

        $this->tpl->assign('deconnexion', 'Déconnexion');
    }

    //J'ai rajouté un paramètre pour savoir si la connexion a généré une erreur ou pas
    //Par défaut aucune erreur
    public function genererAffichageFiche($valeurs = null, $error = 0) {

        $this->valeurs = $valeurs;
        $this->chargementValeurs();
        $this->tpl->assign('authentification', $this->valeurs);
        //J'assigne à une variable $error la valeur du deuxième paramètre
        $this->tpl->assign('error', $error);
        $this->tpl->assign('action', 'connexion');
        $this->tpl->display('mod_authentification/vue/authentificationVue.tpl');

        //Fin méthode
        //Fin méthode
    }

}
