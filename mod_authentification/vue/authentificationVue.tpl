<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{$titre|upper}</title>

        <link rel="icon" type="image/png" href="ressources/public/assets/logolpm.png" />

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="ressources/public/css/style.css" rel="stylesheet">

    </head>
    <body>
        <div class="container-fluid" >
            <div class="row" style="background: linear-gradient(to bottom, rgba(92, 77, 66, 0.2) 0%, rgba(92, 77, 66, 0.8) 100%),url('./ressources/public/assets/img/brew-1031484_1920.jpg');
                 background-position: center;
                 background-repeat: no-repeat;
                 background-attachment: scroll;
                 background-size: cover;
                 min-height: 100vh;
                 color : white;">
                <div class="col-md-4">
                </div>
                <div class="col-md-4 my-auto" id='card'>


                    <div class="card-transparent py-3 "   >
                        <h3 class="card-header text-center">
                            {$titreGestion}</h3>
                        <div class="card-body" >
                            <div class="alert alert-dismissable alert-danger" data-autohide="false"  >

                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >
                                    ×
                                </button>
                                <strong>{$message}</strong>
                            </div>
                            <form role="form" action="index.php" method="POST" >
                                <input type="hidden" name="gestion" value="authentification">
                                <input type="hidden" name="action" value="{$action}">
                                <div class="form-group">
                                    <label for="login">
                                        Login :
                                    </label>

                                    <input class="form-control" type="text" name="f_login" maxlength="255" value="{$authentification->getF_login()}">

                                </div>


                                <div class="form-group">
                                    <label for="pw">
                                        Mot de passe :
                                    </label>

                                    <input autocomplete="off" class="form-control" type="password" name="f_motdepasse"  maxlength="255" value="">
                                </div>


                                <div class="form-group ">
                                    <label for="valider">
                                    </label>
                                    <input type="submit"  class="btn btn-danger  " id='validConnexion'  name="valider" value="Connexion">

                                </div>

                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                </div>
            </div>
        </div>



        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
        <!-- Core theme JS-->

        <script src="ressources/public/js/script.js" type="text/javascript"></script>
        <script>

            //Seul le js présent dans le TPL a accès aux variables Smarty
            //Si j'ai une erreur alors je montre le msg d'alert
            let err = {$error};
            if (err == 1) {
                $('.alert').show();

            } else {
                $('.alert').hide();
            }
            ;


        </script>
    </body>
</html>