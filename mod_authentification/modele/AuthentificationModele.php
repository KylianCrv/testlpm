<?php

class AuthentificationModele extends Modele {

    private $parametre;

    public function __construct($parametre) {
        $this->parametre = $parametre;
    }

    public function rechercherUtilisateur(AuthentificationTable $authEnCours) {

        $sql = 'SELECT * FROM utilisateur JOIN type_utilisateur ON utilisateur.idTypeUtilisateur = type_utilisateur.idTypeUtilisateur '
                . 'WHERE loginUtilisateur = ?';
        $this->idRequete = $this->executeQuery($sql, array($authEnCours->getF_login()));
        $authExistant = $this->idRequete->fetch(PDO::FETCH_ASSOC);

        if ($authEnCours->getF_login() == $authExistant['loginUtilisateur'] && $authEnCours->getF_motdepasse() == $authExistant['motDePasseUtilisateur']) {
// Création de la session
            $_SESSION['login'] = $authEnCours->getF_login();
            $_SESSION['prenomNom'] = $authExistant['nomPrenomUtilisateur'];
            $_SESSION['role'] = $authExistant['designationTypeUtilisateur'];

            return true;
        }
        AuthentificationTable::setMessageErreur("Authentification invalide !");
        return false;
    }

}
