<?php

session_start();
require_once './ressources/configuration.php';
error_reporting(E_ALL ^ E_STRICT);
ini_set('display_errors', 'On');
/**
 * class statique Autoloader, execute la methode inscrire
 * chargement automatique des classes
 */
Autoloader::inscrire();


//traitement de l'authentification
if (!isset($_SESSION['login'])) {
    $_REQUEST['gestion'] = 'authentification';
} elseif (!isset($_REQUEST['gestion'])) {

    $_REQUEST['gestion'] = 'Accueil';
}

//require_once('mod_' . $_REQUEST['gestion'] . '/' . $_REQUEST['gestion'] . '.php');

$oRouteur = new $_REQUEST['gestion']($_REQUEST);

$oRouteur->choixAction();
