<?php

/**
 * Description of accueilVue
 *
 * @author ishish
 */
class AccueilVue {

    private $tpl; //objet smarty

    function __construct() {
        $this->tpl = new Smarty();
    }

    function chargementValeurs() {
        $this->tpl->assign('titre', 'Gestion La Petite Mousse');
        $this->tpl->assign('deconnexion', 'Déconnexion');
        $this->tpl->assign('user', $_SESSION['prenomNom']);
        $this->tpl->assign('role', $_SESSION['role']);
    }

    function genererAffichage() {
        $this->chargementValeurs();
        $this->tpl->display('mod_accueil/vue/accueilVue2.tpl');
    }

}
