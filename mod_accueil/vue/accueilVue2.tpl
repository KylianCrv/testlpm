<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{$titre|upper}</title>

        <link rel="icon" type="image/png" href="ressources/public/assets/logolpm.png" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link href="ressources/public/css/style.css" rel="stylesheet">
        <script src="ressources/public/js/script.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="container-fluid p-0">

            <div class="row no-gutters">
                <div class="col-md-12">
                    {if $role eq 'Gestionnaire'}
                        {include file='ressources/public/menu_gestionnaire.tpl'}
                    {/if}
                    {if $role eq 'Salarié'}
                        {include file='ressources/public/menu_salarie.tpl'}
                    {/if}
                    {if $role eq 'Administrateur'}
                        {include file='ressources/public/menu_administrateur.tpl'}
                    {/if}
                    <div class="row h-100 align-items-center justify-content-center text-center">
                        <div class="col-lg-10 align-self-end">

                            <img src="ressources/public/assets/img/Lpm-logotype.png" alt=""/>
                            <hr class="divider m-7" />
                        </div>
                        <div class="col-lg-8 align-self-baseline">
                            <p class="text-white-75 font-weight-light mb-5"><strong>Bienvenue {$user} sur l'intranet de La Petite Mousse !</strong> <p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        {include file='ressources/public/piedPage.tpl'}
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
        <!-- Core theme JS-->
        <script src="ressources/public/js/jqueryvalidate.js" type="text/javascript"></script>
        <script src="ressources/public/js/validateform.js" type="text/javascript"></script>

    </body>
</html>