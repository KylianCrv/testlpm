<?php

/**
 * routeur du module accueil
 */
class Accueil {

    private $parametre = array();
    private $oControleur; // Attention cette propriété est un objet

    public function __construct($parametre) {

        $this->parametre = $parametre;

        //création d'une instance/objet controleur
        $ctrlMetier = $parametre['gestion'] . 'Controleur';
        $this->oControleur = new $ctrlMetier();
        //$this->oControleur = new AccueilControleur;
    }

    function choixAction() {
        //par défaut, sans action de precisée
        //affichage par défaut de la liste
        $this->oControleur->liste();
    }

}

//}

