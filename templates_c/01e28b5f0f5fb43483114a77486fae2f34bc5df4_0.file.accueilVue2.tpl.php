<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-17 23:36:23
  from 'C:\wamp64\www\testlpm\mod_accueil\vue\accueilVue2.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e9a3d772022f0_65745919',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '01e28b5f0f5fb43483114a77486fae2f34bc5df4' => 
    array (
      0 => 'C:\\wamp64\\www\\testlpm\\mod_accueil\\vue\\accueilVue2.tpl',
      1 => 1587166550,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ressources/public/menu_gestionnaire.tpl' => 1,
    'file:ressources/public/menu_salarie.tpl' => 1,
    'file:ressources/public/menu_administrateur.tpl' => 1,
    'file:ressources/public/piedPage.tpl' => 1,
  ),
),false)) {
function content_5e9a3d772022f0_65745919 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>

        <link rel="icon" type="image/png" href="ressources/public/assets/logolpm.png" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link href="ressources/public/css/style.css" rel="stylesheet">
        <?php echo '<script'; ?>
 src="ressources/public/js/script.js" type="text/javascript"><?php echo '</script'; ?>
>
    </head>
    <body>
        <div class="container-fluid p-0">

            <div class="row no-gutters">
                <div class="col-md-12">
                    <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Gestionnaire') {?>
                        <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/menu_gestionnaire.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Salarié') {?>
                        <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/menu_salarie.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Administrateur') {?>
                        <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/menu_administrateur.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                    <?php }?>
                    <div class="row h-100 align-items-center justify-content-center text-center">
                        <div class="col-lg-10 align-self-end">

                            <img src="ressources/public/assets/img/Lpm-logotype.png" alt=""/>
                            <hr class="divider m-7" />
                        </div>
                        <div class="col-lg-8 align-self-baseline">
                            <p class="text-white-75 font-weight-light mb-5"><strong>Bienvenue <?php echo $_smarty_tpl->tpl_vars['user']->value;?>
 sur l'intranet de La Petite Mousse !</strong> <p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/piedPage.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <!-- Bootstrap core JS-->
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"><?php echo '</script'; ?>
>
        <!-- Third party plugin JS-->
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"><?php echo '</script'; ?>
>
        <!-- Core theme JS-->
        <?php echo '<script'; ?>
 src="ressources/public/js/jqueryvalidate.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="ressources/public/js/validateform.js" type="text/javascript"><?php echo '</script'; ?>
>

    </body>
</html><?php }
}
