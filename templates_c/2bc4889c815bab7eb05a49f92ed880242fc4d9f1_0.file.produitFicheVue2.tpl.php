<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-16 18:34:41
  from 'C:\wamp64\www\testlpm\mod_produit\vue\produitFicheVue2.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e98a541bb6b06_28163940',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2bc4889c815bab7eb05a49f92ed880242fc4d9f1' => 
    array (
      0 => 'C:\\wamp64\\www\\testlpm\\mod_produit\\vue\\produitFicheVue2.tpl',
      1 => 1587062075,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ressources/public/menu_gestionnaire.tpl' => 1,
    'file:ressources/public/menu_salarie.tpl' => 1,
    'file:ressources/public/menu_administrateur.tpl' => 1,
    'file:ressources/public/piedPage.tpl' => 1,
  ),
),false)) {
function content_5e98a541bb6b06_28163940 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\testlpm\\ressources\\libs\\plugins\\function.html_options.php','function'=>'smarty_function_html_options',),1=>array('file'=>'C:\\wamp64\\www\\testlpm\\ressources\\libs\\plugins\\modifier.capitalize.php','function'=>'smarty_modifier_capitalize',),));
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>

        <link rel="icon" type="image/png" href="ressources/public/assets/logolpm.png" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link href="ressources/public/css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid p-0">

            <div class="row no-gutters">
                <div class="col-md-12">
                    <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Gestionnaire') {?>
                        <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/menu_gestionnaire.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Salarié') {?>
                        <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/menu_salarie.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Administrateur') {?>
                        <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/menu_administrateur.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                    <?php }?>

                    <div class="row">
                        <div class="col-md-4 p-3 space">
                            <a href="index.php"><img src="ressources/public/assets/img/logolpm.png" ></a>
                        </div>
                        <div class="col-md-6 p-3 space">
                            <h3><?php echo $_smarty_tpl->tpl_vars['titreGestion']->value;?>
</h3>
                        </div>
                        <div class="col-md-2 p-3 space">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-8">
                            <div class="col-md-offset-2 col-10 col-md-8 col-md-offset-2 space">
                                <form action="index.php" method="post" novalidate="">

                                    <input type="hidden" name="gestion" value="produit">
                                    <input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">




                                    <?php if ($_smarty_tpl->tpl_vars['action']->value != 'ajouter') {?>
                                        <div class="form-group">
                                            Identifiant bière :
                                            <input class="form-control" id="idBiere" name="idBiere" type="text" value="<?php echo $_smarty_tpl->tpl_vars['uneBiere']->value->getIdBiere();?>
" readonly>
                                        </div>
                                    <?php }?>

                                    <div class="form-group">
                                        Nom de la bière <sup>(*)</sup> :
                                        <strong>
                                            <input class="form-control" id="designationBiere" name="designationBiere" type="text" value="<?php echo $_smarty_tpl->tpl_vars['uneBiere']->value->getDesignationBiere();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                                        </strong>
                                    </div>

                                    <div class="form-group">
                                        Taux d'alcool de la bière :
                                        <input class="form-control" id="degresAlcoolBiere" name="degresAlcoolBiere" type="text" value="<?php echo $_smarty_tpl->tpl_vars['uneBiere']->value->getDegresAlcoolBiere();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >
                                    </div>

                                    <?php if ($_smarty_tpl->tpl_vars['action']->value == 'consulter' || $_smarty_tpl->tpl_vars['action']->value == 'supprimer') {?>
                                        <div class="form-group">
                                            Type de fermentation de la bière :

                                            <input class="form-control" id="nomFermentationBiere" name="nomFermentationBiere" type="text" value="<?php echo $_smarty_tpl->tpl_vars['uneBiere']->value->getNomFermentationBiere();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 ></div>
                                        <?php } else { ?>
                                        <div class="dropdown">
                                            <button class='btn btn-secondary dropdown-toggle my-2' type="button" id="dropdownMenuButton" data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                Type de fermentation de la bière :
                                            </button>
                                            <select class="dropdown-menu" name="idStyleBiere" size="<?php echo count($_smarty_tpl->tpl_vars['fermentations']->value);?>
">
                                                <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['fermentations']->value,'selected'=>$_smarty_tpl->tpl_vars['fermentationSelected']->value),$_smarty_tpl);?>

                                            </select>
                                        </div>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['action']->value == 'consulter' || $_smarty_tpl->tpl_vars['action']->value == 'supprimer') {?>
                                        <div class="form-group">
                                            Style de la bière :

                                            <input class="form-control" id="nomStyleBiere" name="nomStyleBiere" type="text" value="<?php echo $_smarty_tpl->tpl_vars['uneBiere']->value->getNomStyleBiere();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 ></div>
                                        <?php } else { ?>
                                        <div class="dropdown">
                                            <button class='btn btn-secondary dropdown-toggle my-2' type="button" id="dropdownMenuButton" data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                Style de la bière :
                                            </button>
                                            <select class="dropdown-menu" name="idStyleBiere" size="<?php echo count($_smarty_tpl->tpl_vars['styles']->value);?>
">
                                                <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['styles']->value,'selected'=>$_smarty_tpl->tpl_vars['styleSelected']->value),$_smarty_tpl);?>

                                            </select>
                                        </div>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['action']->value == 'consulter' || $_smarty_tpl->tpl_vars['action']->value == 'supprimer') {?>
                                        <div class="form-group">
                                            Couleur de la bière :

                                            <input class="form-control" id="nomCouleurBiere" name="nomCouleurBiere" type="text" value="<?php echo $_smarty_tpl->tpl_vars['uneBiere']->value->getNomCouleurBiere();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 ></div>
                                        <?php } else { ?>
                                        <div class="dropdown">
                                            <button class='btn btn-secondary dropdown-toggle my-2' type="button" id="dropdownMenuButton" data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                Couleur de la bière :
                                            </button>
                                            <select class="dropdown-menu" name="idCouleurBiere" size="<?php echo count($_smarty_tpl->tpl_vars['couleurs']->value);?>
">
                                                <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['couleurs']->value,'selected'=>$_smarty_tpl->tpl_vars['couleurSelected']->value),$_smarty_tpl);?>

                                            </select>
                                        </div>
                                    <?php }?>

                                    <div class="form-group">

                                        <div class="col-md-8">
                                            <input type="button"  class="btn btn-danger btn-sm"
                                                   onclick='location.href = "index.php?gestion=produit"' value="Retour">
                                        </div>

                                    </div>


                            </div>
                            <div class="col-md-2 align-self-end pb-3">

                                <?php if ($_smarty_tpl->tpl_vars['action']->value != 'consulter') {?>
                                    <div class="col-md-4">
                                        <input type="submit" class="btn btn-danger btn-sm" value="<?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['action']->value);?>
">
                                    </div>
                                <?php }?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/piedPage.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
            <!-- Bootstrap core JS-->
            <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"><?php echo '</script'; ?>
>
            <!-- Third party plugin JS-->
            <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"><?php echo '</script'; ?>
>
            <!-- Core theme JS-->
            <?php echo '<script'; ?>
 src="js/scripts.js"><?php echo '</script'; ?>
>
    </body>
</html><?php }
}
