<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-18 21:08:16
  from 'C:\wamp64\www\testlpm\mod_authentification\vue\authentificationVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e9b6c40b53cd9_26303092',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'accd4cb8cc20e8237ad2d2983f93d2252ffdfd9b' => 
    array (
      0 => 'C:\\wamp64\\www\\testlpm\\mod_authentification\\vue\\authentificationVue.tpl',
      1 => 1587244081,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e9b6c40b53cd9_26303092 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>

        <link rel="icon" type="image/png" href="ressources/public/assets/logolpm.png" />

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link href="ressources/public/css/style.css" rel="stylesheet">

    </head>
    <body>
        <div class="container-fluid" >
            <div class="row" style="background: linear-gradient(to bottom, rgba(92, 77, 66, 0.2) 0%, rgba(92, 77, 66, 0.8) 100%),url('./ressources/public/assets/img/brew-1031484_1920.jpg');
                 background-position: center;
                 background-repeat: no-repeat;
                 background-attachment: scroll;
                 background-size: cover;
                 min-height: 100vh;
                 color : white;">
                <div class="col-md-4">
                </div>
                <div class="col-md-4 my-auto" id='card'>


                    <div class="card-transparent py-3 "   >
                        <h3 class="card-header text-center">
                            <?php echo $_smarty_tpl->tpl_vars['titreGestion']->value;?>
</h3>
                        <div class="card-body" >
                            <div class="alert alert-dismissable alert-danger" data-autohide="false"  >

                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >
                                    ×
                                </button>
                                <strong><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</strong>
                            </div>
                            <form role="form" action="index.php" method="POST" >
                                <input type="hidden" name="gestion" value="authentification">
                                <input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">
                                <div class="form-group">
                                    <label for="login">
                                        Login :
                                    </label>

                                    <input class="form-control" type="text" name="f_login" maxlength="255" value="<?php echo $_smarty_tpl->tpl_vars['authentification']->value->getF_login();?>
">

                                </div>


                                <div class="form-group">
                                    <label for="pw">
                                        Mot de passe :
                                    </label>

                                    <input autocomplete="off" class="form-control" type="password" name="f_motdepasse"  maxlength="255" value="">
                                </div>


                                <div class="form-group ">
                                    <label for="valider">
                                    </label>
                                    <input type="submit"  class="btn btn-danger  " id='validConnexion'  name="valider" value="Connexion">

                                </div>

                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                </div>
            </div>
        </div>



        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"><?php echo '</script'; ?>
>
        <!-- Third party plugin JS-->
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"><?php echo '</script'; ?>
>
        <!-- Core theme JS-->

        <?php echo '<script'; ?>
 src="ressources/public/js/script.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
>

            //Seul le js présent dans le TPL a accès aux variables Smarty
            //Si j'ai une erreur alors je montre le msg d'alert
            let err = <?php echo $_smarty_tpl->tpl_vars['error']->value;?>
;
            if (err == 1) {
                $('.alert').show();

            } else {
                $('.alert').hide();
            }
            ;


        <?php echo '</script'; ?>
>
    </body>
</html><?php }
}
