<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-15 20:32:10
  from 'C:\wamp64\www\testlpm\mod_produit\vue\produitListeVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e976f4a626113_51309595',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fcbc7593b959a9a4657764fd7fff41aeee32617c' => 
    array (
      0 => 'C:\\wamp64\\www\\testlpm\\mod_produit\\vue\\produitListeVue.tpl',
      1 => 1586902279,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ressources/public/menu_gestionnaire.tpl' => 1,
    'file:ressources/public/menu_salarie.tpl' => 1,
    'file:ressources/public/piedPage.tpl' => 1,
  ),
),false)) {
function content_5e976f4a626113_51309595 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>

        <link rel="icon" type="image/png" href="ressources/public/assets/logolpm.png" />

        <link href="ressources/public/css/styles.css" rel="stylesheet">

    </head>
    <body>
        <header class='masthead'>
            <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Gestionnaire') {?>
                <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/menu_gestionnaire.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Salarié') {?>
                <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/menu_salarie.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
            <?php }?>



            <div class="row">
                <div class="col-md-4 space">
                    <a href="index.php?gestion"><img src="ressources/public/assets/img/logolpm.png" ></a>
                </div>
                <div class="col-md-6 space">
                    <h3><?php echo $_smarty_tpl->tpl_vars['titreGestion']->value;?>
</h3>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Gestionnaire') {?><div class="col-md-2 space">

                        <form action='index.php' method='post'>
                            <input type='hidden' name='gestion' value='produit'>
                            <input type='hidden' name='action' value='form_ajouter'>

                            Ajouter un produit :
                            <input type="submit"  class="btn btn-warning btn-sm"  name="ajouter" value="Ajouter">
                        </form>

                    </div>
                <?php }?>
            </div>


            <div class="row">
                <div class="col-md-offset-1 col-md-10 col-md-offset-1">
                    <p <?php if ($_smarty_tpl->tpl_vars['message']->value != '') {?> class='pos-message'<?php }?>>
                        <?php echo $_smarty_tpl->tpl_vars['message']->value;?>

                    </p>

                </div>
            </div>
            <div class="row">
                <!-- ICI LES DONNES  -->
                <div class="col-md-offset-1 col-md-12 col-md-offset-1 h-100 align-items-center justify-content-center text-center">


                    <table class="table" style='color: white;'>
                        <thead class="">
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    NOM
                                </th>
                                <th>
                                    COULEUR
                                </th>
                                <th>
                                    ACTION
                                </th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td colspan="6">
                                    <p>INFORMATIONS : ... </p>
                                </td>
                            </tr>

                        </tfoot>
                        <tbody>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listeProduits']->value, 'produit');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['produit']->value) {
?>
                                <tr>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['produit']->value['idBiere'];?>

                                    </td>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['produit']->value['designationBiere'];?>

                                    </td>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['produit']->value['nomCouleurBiere'];?>

                                    </td>

                                    <td>
                                        <form action='index.php' method='post'>
                                            <input type='hidden' name='idBiere' value='<?php echo $_smarty_tpl->tpl_vars['produit']->value['idBiere'];?>
'>
                                            <input type='hidden' name='gestion' value='produit'>
                                            <input type='hidden' name='action' value='form_consulter'>

                                            <input type="submit"  class="btn btn-warning btn-sm" name="consulter" value="Consulter">
                                        </form>
                                        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Gestionnaire') {?>
                                            <form action='index.php' method='post'>
                                                <input type='hidden' name='idBiere' value='<?php echo $_smarty_tpl->tpl_vars['produit']->value['idBiere'];?>
'>
                                                <input type='hidden' name='gestion' value='produit'>
                                                <input type='hidden' name='action' value='form_modifier'>

                                                <input type="submit"  class="btn btn-warning btn-sm"   name="modifier" value="Modifier">
                                            </form>

                                            <form action='index.php' method='post'>
                                                <input type='hidden' name='idBiere' value='<?php echo $_smarty_tpl->tpl_vars['produit']->value['idBiere'];?>
'>
                                                <input type='hidden' name='gestion' value='produit'>
                                                <input type='hidden' name='action' value='form_supprimer'>

                                                <input type="submit"  class="btn btn-warning btn-sm"   name="supprimer" value="Supprimer">
                                            </form>
                                        <?php }?>
                                    </td>
                                </tr>
                            <?php
}
} else {
?>
                                <tr>
                                    <td colspan='6'>Aucun enregistrement de trouvé.</td>
                                </tr>
                            <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                        </tbody>
                    </table>
                </div>
            </div>




        </header>
        <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/piedPage.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"><?php echo '</script'; ?>
>
        <!-- Third party plugin JS-->
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"><?php echo '</script'; ?>
>
        <!-- Core theme JS-->
        <?php echo '<script'; ?>
 src="../../ressources/public/js/scripts.js"><?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}
