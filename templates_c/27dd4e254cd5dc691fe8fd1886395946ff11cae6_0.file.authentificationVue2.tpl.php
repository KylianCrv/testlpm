<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-16 07:29:42
  from 'C:\wamp64\www\testlpm\mod_authentification\vue\authentificationVue2.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e980966de90a1_09971222',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '27dd4e254cd5dc691fe8fd1886395946ff11cae6' => 
    array (
      0 => 'C:\\wamp64\\www\\testlpm\\mod_authentification\\vue\\authentificationVue2.tpl',
      1 => 1586968453,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ressources/public/piedPage.tpl' => 1,
  ),
),false)) {
function content_5e980966de90a1_09971222 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['titre']->value, 'UTF-8');?>
</title>

        <link rel="icon" type="image/png" href="ressources/public/assets/logolpm.png" />


        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="ressources/public/css/styles.css" rel="stylesheet">
    </head>
    <body>
        <header class='masthead'>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">


                        <div class="card-transparent py-3 "  id='card' >
                            <h3 class="card-header text-center">
                                <?php echo $_smarty_tpl->tpl_vars['titreGestion']->value;?>
</h3>
                            <div class="card-body" >
                                <div class="alert alert-dismissable alert-warning" data-autohide="false"  >

                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >
                                        ×
                                    </button>
                                    <strong><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</strong>
                                </div>
                                <form role="form" action="index.php" method="POST" >
                                    <input type="hidden" name="gestion" value="authentification">
                                    <input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">
                                    <div class="form-group">
                                        <label for="login">
                                            Login :
                                        </label>

                                        <input class="form-control" type="text" name="f_login" maxlength="255" value="<?php echo $_smarty_tpl->tpl_vars['authentification']->value->getF_login();?>
">
                                    </div>


                                    <div class="form-group">
                                        <label for="pw">
                                            Mot de passe :
                                        </label>

                                        <input autocomplete="off" class="form-control" type="password" name="f_motdepasse"  maxlength="255" value="">
                                    </div>


                                    <div class="form-group ">
                                        <label for="valider">
                                        </label>
                                        <input type="submit"  class="btn btn-primary  " id='validConnexion'  name="valider" value="Connexion">

                                    </div>

                                </form>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </div>
        </header>
        <footer>
            <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/piedPage.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        </footer>

        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"><?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"><?php echo '</script'; ?>
>
        <!-- Third party plugin JS-->
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"><?php echo '</script'; ?>
>
        <!-- Core theme JS-->
        <!--script src="./scripts.js" type="text/javascript"><?php echo '</script'; ?>
-->
        <?php echo '<script'; ?>
>
            //Seul le js présent dans le TPL a accès aux variables Smarty
            //Si j'ai une erreur alors je montre le msg d'alert
            let err = <?php echo $_smarty_tpl->tpl_vars['error']->value;?>
;
            if (err == 1){
                $('.alert').show();
            }
            else{
                $('.alert').hide();
            }
        <?php echo '</script'; ?>
>
    </body>
</html><?php }
}
