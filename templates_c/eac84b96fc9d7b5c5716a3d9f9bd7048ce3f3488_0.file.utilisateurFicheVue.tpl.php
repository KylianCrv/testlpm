<?php
/* Smarty version 3.1.34-dev-7, created on 2020-04-15 19:54:11
  from 'C:\wamp64\www\testlpm\mod_utilisateur\vue\utilisateurFicheVue.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e976663e9c391_32214874',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'eac84b96fc9d7b5c5716a3d9f9bd7048ce3f3488' => 
    array (
      0 => 'C:\\wamp64\\www\\testlpm\\mod_utilisateur\\vue\\utilisateurFicheVue.tpl',
      1 => 1586902254,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:ressources/public/menu_gestionnaire.tpl' => 1,
    'file:ressources/public/menu_salarie.tpl' => 1,
    'file:ressources/public/piedPage.tpl' => 1,
  ),
),false)) {
function content_5e976663e9c391_32214874 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\wamp64\\www\\testlpm\\ressources\\libs\\plugins\\modifier.capitalize.php','function'=>'smarty_modifier_capitalize',),));
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Creative - Start Bootstrap Theme</title>
        <!-- Font Awesome icons (free version)-->
        <?php echo '<script'; ?>
 src="https://use.fontawesome.com/releases/v5.12.1/js/all.js" crossorigin="anonymous"><?php echo '</script'; ?>
>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />
        <!-- Third party plugin CSS-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="ressources/public/css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Gestionnaire') {?>
            <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/menu_gestionnaire.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['role']->value == 'Salarié') {?>
            <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/menu_salarie.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <?php }?>
        <!-- Masthead-->
        <header class="masthead">
            <div class="row">
                <div class="col-md-4 space">
                    <a href="index.php"><img src="ressources/public/assets/img/logolpm.png" ></a>
                </div>
                <div class="col-md-6 space">
                    <h3><?php echo $_smarty_tpl->tpl_vars['titreGestion']->value;?>
</h3>
                </div>
                <div class="col-md-2 space">
                </div>
            </div>
            <div class="container h-100">
                <div class="row">
                    <!-- ICI LES DONNES, LE FORMULAIRE (LA FICHE !) -->



                    <div class="col-md-offset-2 col-12 col-md-8 col-md-offset-2 space">
                        <form action="index.php" method="post" novalidate="">

                            <input type="hidden" name="gestion" value="utilisateur">
                            <input type="hidden" name="action" value="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">
                            <input id="motDePasseUtilisateur" name="motDePasseUtilisateur" type="hidden" value="">



                            <?php if ($_smarty_tpl->tpl_vars['action']->value != 'ajouter') {?>
                                <div class="form-group">
                                    Identifiant :
                                    <input class="form-control" id="idUtilisateur" name="idUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getIdUtilisateur();?>
" readonly>
                                </div>
                            <?php }?>

                            <div class="form-group">
                                Nom prénom utilisateur <sup>(*)</sup> :
                                <strong>
                                    <input class="form-control" id="nomPrenomUtilisateur" name="nomPrenomUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getNomPrenomUtilisateur();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 required="required">
                                </strong>
                            </div>

                            <div class="form-group">
                                login :
                                <input class="form-control" id="loginUtilisateur" name="loginUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getLoginUtilisateur();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >
                            </div>
                            <div class="form-group">
                                Type d'utilisateur :
                                <input class="form-control" id="designationTypeUtilisateur" name="designationTypeUtilisateur" type="text" value="<?php echo $_smarty_tpl->tpl_vars['unUtilisateur']->value->getDesignationTypeUtilisateur();?>
" <?php echo $_smarty_tpl->tpl_vars['comportement']->value;?>
 >
                            </div>


                            <div class="form-group">

                                <div class="col-md-11">
                                    <input type="button"  class="btn btn-warning btn-sm"
                                           onclick='location.href = "index.php?gestion=utilisateur"' value="Retour">
                                </div>

                                <?php if ($_smarty_tpl->tpl_vars['action']->value != 'consulter') {?>
                                    <div class="col-md-1">
                                        <input type="submit" class="btn btn-warning btn-sm" value="<?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['action']->value);?>
">
                                    </div>
                                <?php }?>

                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </header>
        <?php $_smarty_tpl->_subTemplateRender('file:ressources/public/piedPage.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <!-- Bootstrap core JS-->
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"><?php echo '</script'; ?>
>
        <!-- Third party plugin JS-->
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"><?php echo '</script'; ?>
>
        <!-- Core theme JS-->
        <?php echo '<script'; ?>
 src="js/scripts.js"><?php echo '</script'; ?>
>
    </body>
</html>
<?php }
}
