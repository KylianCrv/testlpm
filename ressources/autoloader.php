<?php

/**
 * Description of autoloader
 *
 * @author ishish
 */
class Autoloader {

    public static function inscrire() {
        spl_autoload_register(array(__CLASS__, 'autoload'));
    }

    public static function autoload($maClasse) {
// $maClasse accepte le nom de la classe :  Accueil (routeur du module accueil),
// reunion (routeur, controleur, modèle,...)

        $chemins = array(
            'mod_accueil/',
            'mod_accueil/controleur/',
            'mod_accueil/modele/',
            'mod_accueil/vue/',
            'mod_produit/',
            'mod_produit/controleur/',
            'mod_produit/modele/',
            'mod_produit/vue/',
            'mod_authentification/',
            'mod_authentification/controleur/',
            'mod_authentification/modele/',
            'mod_authentification/vue/',
            'mod_utilisateur/',
            'mod_utilisateur/controleur/',
            'mod_utilisateur/modele/',
            'mod_utilisateur/vue/',
            'mod_client/',
            'mod_client/controleur/',
            'mod_client/modele/',
            'mod_client/vue/'
        );
        foreach ($chemins as $chemin) {
            if (file_exists($chemin . $maClasse . '.php')) {
                require_once ($chemin . $maClasse . '.php');
                return;
            }
        }
    }

}
