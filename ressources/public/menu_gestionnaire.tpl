<nav class="navbar navbar-expand-lg navbar-light bg-light navbar-dark bg-dark static-top" id="mainNav">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php">Accueil</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?gestion=produit">Nos produits</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Mon espace personnel
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="index.php?gestion=authentification&action=deconnexion">{$deconnexion}</a>


                    </div>
                </li>


            </ul>
        </div>
    </div>
</nav>