<?php

header('Content-Type: application/json');
error_reporting(E_ALL ^ E_STRICT);
ini_set('display_errors', 'On');
//msg d'erreur par défaut
$res['error'] = 'Un paramètre est manquant.';

//test si param est là
if (isset($_POST['nomPrenom']) && isset($_POST['tel']) && isset($_POST['email'])) {
    $res['error'] = '';

    $to = $_POST['email'];
    $subject = 'Inscription à La Petite Mousse';
    $message = 'Bienvenue ' + $_POST['nomPrenom'] + ',\n Votre inscription à la Petite Mousse a bien été effectué.';
//    if (mail($to, $subject, $message)) {
    if (true) {
        $res['success'] = 'L\'envoi du mail est ok.';
    } else {

        $res['error'] = 'L\'envoi du mail a echoué';
    }
}

echo json_encode($res);
