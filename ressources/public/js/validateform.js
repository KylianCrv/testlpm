'use strict';
jQuery.validator.addMethod('testLetter', function (value, element) {
    return value.includes('@');
}, 'Veuillez saisir une adresse email correcte.');


$('#formulaire').validate({
    // on click : true
    rules: {
        nomPrenomClient: {
            required: true,
            minlength: 3
        },
        adresseClient: {
            required: true,
            minlength: 3
        },
        telClient: {
            required: true,
            minlength: 3
        },
        emailClient: {
            email: true,
            required: true,
            testLetter: true
        }
    },
    messages: {
        nomPrenomClient: {
            required: "Veuillez saisir le nom prénom du client svp",
            minlength: "Veuilez saisir au moins 3 caractères"
        },
        adresseClient: {
            required: "Veuillez saisir l'adresse du client svp",
            minlength: "Veuilez saisir au moins 3 caractères"
        },
        telClient: {
            required: "Veuillez saisir le téléphone du client svp",
            minlength: "Veuilez saisir au moins 3 caractères"
        },
        emailClient: {
            required: "Veuillez saisir l'email du client svp",
            email: "Veuilez saisir un email correct",
            testLetter: "Veuillez saisir une adresse email correcte."
        }
    }
});



