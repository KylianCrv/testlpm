<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of utilisateurModele
 *
 * @author ishish
 */
class UtilisateurModele extends Modele {

    private $parametre;

    public function __construct($parametre) {

        $this->parametre = $parametre;
    }

    public function getListeUtilisateurs() {

//Requête attendue de type SELECT (liste des lieux)
        $sql = "SELECT * FROM utilisateur JOIN type_utilisateur ON utilisateur.idTypeUtilisateur = type_utilisateur.idTypeUtilisateur";

        $idRequete = $this->executeQuery($sql);

        return $idRequete->fetchall(PDO::FETCH_ASSOC);
    }

    public function getUnUtilisateur() {
        $sql = "SELECT * FROM utilisateur"
                . " JOIN type_utilisateur ON utilisateur.idTypeUtilisateur = type_utilisateur.idTypeUtilisateur"
                . " WHERE idUtilisateur = ?";
        $idRequete = $this->executeQuery($sql, array($this->parametre['idUtilisateur']));
        $user = new UtilisateurTable($idRequete->fetch());

        return $user;
    }

    public function addUtilisateur(UtilisateurTable $valeurs) {
// Requête de type Insert (création)


        $sql = 'INSERT INTO utilisateur (nomPrenomUtilisateur, loginUtilisateur, idTypeUtilisateur, motDePasseUtilisateur)'
                . 'VALUES (?,?,?,?)';

        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getNomPrenomUtilisateur(),
            $valeurs->getLoginUtilisateur(),
            $valeurs->getIdTypeUtilisateur(),
            $valeurs->getMotDePasseUtilisateur()
        ));

        if ($idRequete) {
            UtilisateurTable::setMessageSucces("Création effectuée avec succès !");
        }
    }

    public function editUtilisateur(UtilisateurTable $valeurs) {
// Requête de type Insert (création)
        $sql = "UPDATE utilisateur SET nomPrenomUtilisateur = ?, loginUtilisateur = ?,"
                . " idTypeUtilisateur = ?"
                . "WHERE idUtilisateur = ?";


        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getNomPrenomUtilisateur(),
            $valeurs->getLoginUtilisateur(),
            $valeurs->getIdTypeUtilisateur(),
            $valeurs->getIdUtilisateur()
        ));

        if ($idRequete) {
            UtilisateurTable::setMessageSucces("Modification effectuée avec succès !");
        }
    }

    public function deleteUtilisateur() {

        $sql = "DELETE FROM utilisateur WHERE idUtilisateur = ?";


        $idRequete = $this->executeQuery($sql, array($this->parametre['idUtilisateur']));

        if ($idRequete) {

            UtilisateurTable::setMessageSucces("Suppression effectuée avec succès !");
        }
    }

    public function getAllRoles() {

        $sql = "SELECT * FROM type_utilisateur";

        $idRequete = $this->executeQuery($sql);

        return $idRequete->fetchall(PDO::FETCH_ASSOC);
    }

}
