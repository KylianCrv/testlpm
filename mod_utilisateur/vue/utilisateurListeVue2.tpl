<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{$titre|upper}</title>

        <link rel="icon" type="image/png" href="ressources/public/assets/logolpm.png" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link href="ressources/public/css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid p-0">

            <div class="row no-gutters">
                <div class="col-md-12">
                    {if $role eq 'Gestionnaire'}
                        {include file='ressources/public/menu_gestionnaire.tpl'}
                    {/if}
                    {if $role eq 'Salarié'}
                        {include file='ressources/public/menu_salarie.tpl'}
                    {/if}
                    {if $role eq 'Administrateur'}
                        {include file='ressources/public/menu_administrateur.tpl'}
                    {/if}


                    <div class="row">
                        <div class="col-md-4 p-3 space">
                            <a href="index.php"><img src="ressources/public/assets/img/logolpm.png" ></a>
                        </div>
                        <div class="col-md-6 p-3 space">
                            <h1>{$titreGestion}</h1>
                            <p {if $message neq ''} class='pos-message m-2'{/if}>
                                {$message}
                            </p>
                        </div>
                        <div class="col-md-2 p-3 space">

                            <form action='index.php' method='post'>
                                <input type='hidden' name='gestion' value='utilisateur'>
                                <input type='hidden' name='action' value='form_ajouter'>

                                Ajouter un utilisateur :
                                <input type="submit"  class="btn btn-danger btn-sm"  name="ajouter" value="Ajouter">
                            </form>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-offset-1 col-md-12 col-md-offset-1 h-100 text-center">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            NOM PRENOM
                                        </th>
                                        <th>
                                            LOGIN
                                        </th>
                                        <th>
                                            ROLE
                                        </th>
                                        <th class="w-25">
                                            ACTION
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {foreach from=$listeUtilisateurs item=utilisateur}
                                        <tr>
                                            <td>
                                                {$utilisateur.idUtilisateur}
                                            </td>
                                            <td>
                                                {$utilisateur.nomPrenomUtilisateur}
                                            </td>
                                            <td>
                                                {$utilisateur.loginUtilisateur}
                                            </td>
                                            <td>
                                                {$utilisateur.designationTypeUtilisateur}
                                            </td>

                                            <td>
                                                <div class="flex-row d-flex justify-content-center">
                                                    <form action='index.php' method='post'>
                                                        <input type='hidden' name='idUtilisateur' value='{$utilisateur.idUtilisateur}'>
                                                        <input type='hidden' name='gestion' value='utilisateur'>
                                                        <input type='hidden' name='action' value='form_consulter'>

                                                        <input type="submit"  class="btn btn-danger btn-sm mx-1" name="consulter" value="Consulter">
                                                    </form>

                                                    <form action='index.php' method='post'>
                                                        <input type='hidden' name='idUtilisateur' value='{$utilisateur.idUtilisateur}'>
                                                        <input type='hidden' name='gestion' value='utilisateur'>
                                                        <input type='hidden' name='action' value='form_modifier'>

                                                        <input type="submit"  class="btn btn-danger btn-sm mx-1"   name="modifier" value="Modifier">
                                                    </form>

                                                    <form action='index.php' method='post'>
                                                        <input type='hidden' name='idUtilisateur' value='{$utilisateur.idUtilisateur}'>
                                                        <input type='hidden' name='gestion' value='utilisateur'>
                                                        <input type='hidden' name='action' value='form_supprimer'>

                                                        <input type="submit"  class="btn btn-danger btn-sm mx-1"   name="supprimer" value="Supprimer">
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    {foreachelse}
                                        <tr>
                                            <td colspan='6'>Aucun enregistrement de trouvé.</td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        {include file='ressources/public/piedPage.tpl'}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
        <!-- Core theme JS-->
        <script src="../../ressources/public/js/scripts.js"></script>
    </body>
</html>
