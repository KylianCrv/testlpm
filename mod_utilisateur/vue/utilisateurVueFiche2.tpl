<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{$titre|upper}</title>

        <link rel="icon" type="image/png" href="ressources/public/assets/logolpm.png" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link href="ressources/public/css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid p-0">

            <div class="row no-gutters">
                <div class="col-md-12">
                    {if $role eq 'Gestionnaire'}
                        {include file='ressources/public/menu_gestionnaire.tpl'}
                    {/if}
                    {if $role eq 'Salarié'}
                        {include file='ressources/public/menu_salarie.tpl'}
                    {/if}
                    {if $role eq 'Administrateur'}
                        {include file='ressources/public/menu_administrateur.tpl'}
                    {/if}


                    <div class="row">
                        <div class="col-md-4 p-3 space">
                            <a href="index.php"><img src="ressources/public/assets/img/logolpm.png" ></a>
                        </div>
                        <div class="col-md-6 p-3 space">
                            <h1>{$titreGestion}</h1>

                            <p {if $unUtilisateur->getMessageErreur() neq ''} class="pos-messageErreur " {/if}>
                                {$unUtilisateur->getMessageErreur()}
                        </div>


                        <div class="col-md-2 p-3 space">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-8">
                            <form action="index.php" method="post" novalidate="">

                                <input type="hidden" name="gestion" value="utilisateur">
                                <input type="hidden" name="action" value="{$action}">
                                <input id="motDePasseUtilisateur" name="motDePasseUtilisateur" type="hidden" value="">



                                {if $action neq 'ajouter'}
                                    <div class="form-group">
                                        Identifiant :
                                        <input class="form-control" id="idUtilisateur" name="idUtilisateur" type="text" value="{$unUtilisateur->getIdUtilisateur()}" readonly>
                                    </div>
                                {/if}

                                <div class="form-group">
                                    Nom prénom utilisateur <sup>(*)</sup> :
                                    <strong>
                                        <input class="form-control" id="nomPrenomUtilisateur" name="nomPrenomUtilisateur" type="text" value="{$unUtilisateur->getNomPrenomUtilisateur()}" {$comportement} required="required">
                                    </strong>
                                </div>

                                <div class="form-group">
                                    login :
                                    <input class="form-control" id="loginUtilisateur" name="loginUtilisateur" type="text" value="{$unUtilisateur->getLoginUtilisateur()}" {$comportement} >
                                </div>

                                {if $action eq 'consulter' or $action eq 'supprimer'}
                                    <div class="form-group">
                                        Type d'utilisateur :

                                        <input class="form-control" id="designationTypeUtilisateur" name="designationTypeUtilisateur" type="text" value="{$unUtilisateur->getDesignationTypeUtilisateur()}" {$comportement} ></div>
                                    {else}
                                    <div class="dropdown">
                                        <button class='btn btn-secondary dropdown-toggle my-2 w-100' type="button" id="dropdownMenuButton" data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                            Type d'utilisateur :
                                        </button>
                                        <select class="dropdown-menu w-100" name="idTypeUtilisateur" size="{$roles|@count}">
                                            {html_options options=$roles selected=$roleSelected}
                                        </select>
                                    </div>
                                {/if}
                                <div class="form-group">

                                    <div class="col-md-8">
                                        <input type="button"  class="btn btn-danger btn-sm"
                                               onclick='location.href = "index.php?gestion=utilisateur"' value="Retour">
                                    </div>

                                </div>


                        </div>
                        <div class="col-md-2 align-self-end pb-3">

                            {if $action neq 'consulter'}
                                <div class="col-md-4">
                                    <input type="submit" class="btn btn-danger btn-sm" value="{$action|capitalize}">
                                </div>
                            {/if}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {include file='ressources/public/piedPage.tpl'}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
        <!-- Core theme JS-->
        <script src="./scripts.js"></script>

</html>
