<?php

/**
 * Description of utilisateurVue
 *
 * @author ishish
 */
class UtilisateurVue {

    private $parametre; //array
    private $tpl; //objet
    private $valeurs;

    public function __construct($parametre) {

        $this->parametre = $parametre;

        $this->tpl = new Smarty();
    }

    public function chargementValeurs() {

        $this->tpl->assign('titre', 'Gestion La Petite Mousse');

        $this->tpl->assign('deconnexion', 'Déconnexion');
        $this->tpl->assign('role', $_SESSION['role']);
    }

    public function genererAffichageListe($valeurs) {

        $this->valeurs = $valeurs;

        $this->chargementValeurs();

        $this->tpl->assign('titreGestion', 'Liste des utilisateurs');

        $this->tpl->assign('message', UtilisateurTable::getMessageSucces());

        $this->tpl->assign('listeUtilisateurs', $this->valeurs);

        $this->tpl->display('mod_utilisateur/vue/utilisateurListeVue2.tpl');
    }

    public function genererAffichageFiche($valeurs, $roles = null) {

        $this->valeurs = $valeurs;

        $this->chargementValeurs();

        switch ($this->parametre['action']) {

            case 'form_consulter':

                $this->tpl->assign('titreGestion', 'Consultation d\'un utilisateur');

                $this->tpl->assign('action', 'consulter');

                $this->tpl->assign('comportement', 'disabled');

                $this->tpl->assign('unUtilisateur', $this->valeurs);

                break;

            case 'form_ajouter':
            case 'ajouter':

                $this->tpl->assign('titreGestion', 'Création d\'un utilisateur');

                $this->tpl->assign('action', 'ajouter');
                $this->tpl->assign('roles', $roles);

                $this->tpl->assign('roleSelected', 1);
                $this->tpl->assign('comportement', '');

                $this->tpl->assign('unUtilisateur', $this->valeurs);

                break;

            case 'form_modifier':
            case 'modifier':

                $this->tpl->assign('titreGestion', 'Modification d\'un utilisateur');

                $this->tpl->assign('action', 'modifier');
                $this->tpl->assign('roles', $roles);

                $this->tpl->assign('roleSelected', $this->valeurs->getIdTypeUtilisateur());
                $this->tpl->assign('comportement', '');

                $this->tpl->assign('unUtilisateur', $this->valeurs);

                break;

            case 'form_supprimer':

                $this->tpl->assign('titreGestion', 'Suppression d\'un utilisateur');

                $this->tpl->assign('action', 'supprimer');

                $this->tpl->assign('comportement', 'disabled');

                $this->tpl->assign('unUtilisateur', $this->valeurs);

                break;
        }


        $this->tpl->display('mod_utilisateur/vue/utilisateurVueFiche2.tpl');

        //Fin méthode
    }

}
