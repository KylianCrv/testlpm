<?php

/**
 * Description of utilisateurControleur
 *
 * @author ishish
 */
class UtilisateurControleur {

    //put your code heprivate $parametre; //array
    private $oModele; // objet
    private $oVue; // objet

    public function __construct($parametre) {

        $this->parametre = $parametre;
//Création d'un objet modele
        $this->oModele = new UtilisateurModele($this->parametre);
//Création d'un objet vue
        $this->oVue = new UtilisateurVue($this->parametre);
    }

    public function liste() {

        $valeurs = $this->oModele->getListeUtilisateurs();

        $this->oVue->genererAffichageListe($valeurs);
    }

    public function form_consulter() {

        $valeurs = $this->oModele->getUnUtilisateur();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function form_ajouter() {

        $prepareUtilisateur = new UtilisateurTable();
        $roles = $this->getRoleForSelect($this->oModele->getAllRoles());
        $this->oVue->genererAffichageFiche($prepareUtilisateur, $roles);
    }

    public function form_modifier() {

        $valeurs = $this->oModele->getUnUtilisateur();

        $roles = $this->getRoleForSelect($this->oModele->getAllRoles());
        $this->oVue->genererAffichageFiche($valeurs, $roles);
    }

    public function form_supprimer() {

        $valeurs = $this->oModele->getUnUtilisateur();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function ajouter() {

        $controleUtilisateur = new UtilisateurTable($this->parametre);

        if ($controleUtilisateur->getAutorisationBD() == false) {
// ici nous sommes en erreur
            $roles = $this->getRoleForSelect($this->oModele->getAllRoles());
            $this->oVue->genererAffichageFiche($controleUtilisateur, $roles);
        } else {
// ici l'insertion est possible !
            $this->oModele->addUtilisateur($controleUtilisateur);

//Ici l'objet controleur (oControleur)
//Il a été créé dans le routeur
            $this->liste();
        }
    }

    public function modifier() {

        $controleUtilisateur = new UtilisateurTable($this->parametre);

        if ($controleUtilisateur->getAutorisationBD() == false) {
// ici nous sommes en erreur
            $roles = $this->getRoleForSelect($this->oModele->getAllRoles());
            $this->oVue->genererAffichageFiche($controleUtilisateur, $roles);
        } else {
// ici l'édition est possible !
            $this->oModele->editUtilisateur($controleUtilisateur);
//Ici l'objet controleur (oControleur)
//Il a été créé dans le routeur

            $this->liste();
        }
    }

    public function supprimer() {

        $this->oModele->deleteUtilisateur();

        $this->liste();
    }

    private function getRoleForSelect($roles) {
        $tab = array();

        for ($i = 0; $i < count($roles); $i++) {

            $role = $roles[$i];
            $tab[$role['idTypeUtilisateur']] = $role['designationTypeUtilisateur'];
        }
        return $tab;
    }

}
