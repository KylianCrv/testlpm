<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of utilisateurTable
 *
 * @author ishish
 */
class UtilisateurTable {

    private $idUtilisateur = "";
    private $nomPrenomUtilisateur = "";
    private $loginUtilisateur = "";
    private $motDePasseUtilisateur = "petitemousse";
    private $idTypeUtilisateur = "";
    private $designationTypeUtilisateur = "";
    private $autorisationBD = true;
    private static $messageErreur = "";
    private static $messageSucces = "";

    // 2 Importer la méthode hydrater
    public function hydrater(array $row) {

        foreach ($row as $k => $v) {
// Concaténation : nom de la méthode setter à appeler
            $setter = 'set' . ucfirst($k);
// fonction prend 2 paramètres : l'objet en cours et le nom de la méthode
            if (method_exists($this, $setter)) {
// Invoquer la méthode
                $this->$setter($v);
            }
        }
    }

    public function __construct($data = null) {

        if ($data != null) {

            $this->hydrater($data);
        }
    }

    ///////GETTER////////

    function getDesignationTypeUtilisateur() {
        return $this->designationTypeUtilisateur;
    }

    function getIdUtilisateur() {
        return $this->idUtilisateur;
    }

    function getNomPrenomUtilisateur() {
        return $this->nomPrenomUtilisateur;
    }

    function getLoginUtilisateur() {
        return $this->loginUtilisateur;
    }

    function getMotDePasseUtilisateur() {
        return $this->motDePasseUtilisateur;
    }

    function getIdTypeUtilisateur() {
        return $this->idTypeUtilisateur;
    }

    ////////////////SETTER////////////////


    function setDesignationTypeUtilisateur($designationTypeUtilisateur) {
        $this->designationTypeUtilisateur = $designationTypeUtilisateur;
    }

    function setIdUtilisateur($idUtilisateur) {
        $this->idUtilisateur = $idUtilisateur;
    }

    function setNomPrenomUtilisateur($nomPrenomUtilisateur) {
        if (!is_string($nomPrenomUtilisateur) || ctype_space($nomPrenomUtilisateur) || empty($nomPrenomUtilisateur)) {
            self::setMessageErreur("Le nom prénom sont invalides");
            $this->setAutorisationBD(false);
        }

        $this->nomPrenomUtilisateur = $nomPrenomUtilisateur;
    }

    function setLoginUtilisateur($loginUtilisateur) {
        if (!is_string($loginUtilisateur) || ctype_space($loginUtilisateur) || empty($loginUtilisateur)) {
            self::setMessageErreur("Le login est invalide");
            $this->setAutorisationBD(false);
        }

        $this->loginUtilisateur = $loginUtilisateur;
    }

    function setMotDePasseUtilisateur() {
        $pw = $this->motDePasseUtilisateur;
        // technique du salage
        $gauche = 'ar30&y%';
        $droite = 'tk!@';

        $this->motDePasseUtilisateur = hash('ripemd128', "$gauche$pw$droite");
    }

    function setIdTypeUtilisateur($idTypeUtilisateur) {
        $this->idTypeUtilisateur = $idTypeUtilisateur;
    }

/////////////////MESSAGE/////////////////

    static function getMessageErreur() {
        return self::$messageErreur;
    }

    static function getMessageSucces() {
        return self::$messageSucces;
    }

    public static function setMessageErreur($msg) {
        self::$messageErreur = self::$messageErreur . $msg . "<br>";
    }

    public static function setMessageSucces($msg) {
        self::$messageSucces = self::$messageSucces . $msg . "<br>";
    }

/////////////// AUTORISATIONS BD //////////////////
    function getAutorisationBD() {
        return $this->autorisationBD;
    }

    function setAutorisationBD($autorisationBD) {
        $this->autorisationBD = $autorisationBD;
    }

}
