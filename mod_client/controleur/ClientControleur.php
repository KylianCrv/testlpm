<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clientControleur
 *
 * @author Gaël
 */
class ClientControleur {
    
    private $parametre; //array
    private $oModele; // objet
    private $oVue; // objet

    public function __construct($parametre) {

        $this->parametre = $parametre;
//Création d'un objet modele
        $this->oModele = new ClientModele($this->parametre);
//Création d'un objet vue
        $this->oVue = new ClientVue($this->parametre);
    }

    public function liste() {

        $valeurs = $this->oModele->getListeClients();

        $this->oVue->genererAffichageListe($valeurs);
    }

    public function form_consulter() {

        $valeurs = $this->oModele->getUnClient();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function form_ajouter() {

        $prepareClient = new ClientTable();

        $this->oVue->genererAffichageFiche($prepareClient);
    }

    public function form_modifier() {

        $valeurs = $this->oModele->getUnClient();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function form_supprimer() {

        $valeurs = $this->oModele->getUnClient();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function ajouter() {

        $controleClient = new ClientTable($this->parametre);

        if ($controleClient->getAutorisationBD() == false) {
// ici nous sommes en erreur
            $this->oVue->genererAffichageFiche($controleClient);
        } else {
// ici l'insertion est possible !
            $this->oModele->addClient($controleClient);
//Ici l'objet controleur (oControleur)
//Il a été créé dans le routeur
            $this->liste();
        }
    }

    public function modifier() {

        $controleClient = new ClientTable($this->parametre);

        if ($controleClient->getAutorisationBD() == false) {
// ici nous sommes en erreur
            $this->oVue->genererAffichageFiche($controleClient);
        } else {
// ici l'édition est possible !
            $this->oModele->editClient($controleClient);
//Ici l'objet controleur (oControleur)
//Il a été créé dans le routeur

            $this->liste();
        }
    }

    public function supprimer() {

        $this->oModele->deleteClient();

        $this->liste();
    }

}
