<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clientTable
 *
 * @author Gaël
 */
class ClientTable {

    private $idClient = "";
    private $nomPrenomClient = "";
    private $adresseClient = "";
    private $telClient = "";
    private $emailClient = "";
    private $codePostalClient = "";
    private $villeClient = "";
    private $autorisationBD = true;
    private static $messageErreur = "";
    private static $messageSucces = "";

    // 2 Importer la méthode hydrater
    public function hydrater(array $row) {

        foreach ($row as $k => $v) {
            // Concaténation : nom de la méthode setter à appeler
            $setter = 'set' . ucfirst($k);
            // fonction prend 2 paramètres : l'objet en cours et le nom de la méthode
            if (method_exists($this, $setter)) {
                // Invoquer la méthode
                $this->$setter($v);
            }
        }
    }

    public function __construct($data = null) {

        if ($data != null) {

            $this->hydrater($data);
        }
    }

    function getIdClient() {
        return $this->idClient;
    }

    function getNomPrenomClient() {
        return $this->nomPrenomClient;
    }

    function getAdresseClient() {
        return $this->adresseClient;
    }

    function getTelClient() {
        return $this->telClient;
    }

    function getEmailClient() {
        return $this->emailClient;
    }

    function getCodePostalClient() {
        return $this->codePostalClient;
    }

    function getVilleClient() {
        return $this->villeClient;
    }

    function getAutorisationBD() {
        return $this->autorisationBD;
    }

    static function getMessageErreur() {
        return self::$messageErreur;
    }

    static function getMessageSucces() {
        return self::$messageSucces;
    }

    function setIdClient($idClient) {
        $this->idClient = $idClient;
    }

    function setNomPrenomClient($nomPrenomClient) {
        if (ctype_space($nomPrenomClient) || empty($nomPrenomClient) || is_numeric($nomPrenomClient)) {
            self::setMessageErreur("Le nom et prénom du client est incorrect");
            $this->setAutorisationBD(false);
        }
        $this->nomPrenomClient = $nomPrenomClient;
    }

    function setAdresseClient($adresseClient) {
        if (ctype_space($adresseClient) || empty($adresseClient)) {
            self::setMessageErreur("L'adresse du client est incorrecte");
            $this->setAutorisationBD(false);
        }
        $this->adresseClient = $adresseClient;
    }

    function setTelClient($telClient) {
        if (ctype_space($telClient) || empty($telClient)) {
            self::setMessageErreur("Le téléphone du client est incorrect");
            $this->setAutorisationBD(false);
        }
        $this->telClient = $telClient;
    }

    function setCodePostalClient($codePostalClient) {
        if (ctype_space($codePostalClient) || empty($codePostalClient)) {
            self::setMessageErreur("Le code postal du client est incorrect");
            $this->setAutorisationBD(false);
        }
        $this->codePostalClient = $codePostalClient;
    }

    function setVilleClient($villeClient) {
        if (ctype_space($villeClient) || empty($villeClient)) {
            self::setMessageErreur("La ville du client est incorrect");
            $this->setAutorisationBD(false);
        }
        $this->villeClient = $villeClient;
    }

    function setEmailClient($emailClient) {
        if (ctype_space($emailClient) || empty($emailClient)) {
            self::setMessageErreur("L'email du client est incorrect");
            $this->setAutorisationBD(false);
        }
        $this->emailClient = $emailClient;
    }

    function setAutorisationBD($autorisationBD) {
        $this->autorisationBD = $autorisationBD;
    }

    static function setMessageErreur($msg) {
        self::$messageErreur = self::$messageErreur . $msg . "<br>";
    }

    static function setMessageSucces($msg) {
        self::$messageSucces = self::$messageSucces . $msg . "<br>";
    }

}
