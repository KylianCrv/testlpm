<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clientModele
 *
 * @author Gaël
 */
class ClientModele extends Modele {

    private $parametre;

    public function __construct($parametre) {

        $this->parametre = $parametre;
    }

    public function getListeClients() {

        //Requête attendue de type SELECT (liste des lieux)
        $sql = "SELECT * FROM client";


        $idRequete = $this->executeQuery($sql);

        return $idRequete->fetchall(PDO::FETCH_ASSOC);
    }

    public function getUnClient() {

        //Requête attendue de type SELECT (un seul lieu)
        $sql = 'SELECT * FROM client WHERE idClient = ?';

        $idRequete = $this->executeQuery($sql, array($this->parametre['idClient']));

        //     var_dump($idRequete->fetch());
        $client = new ClientTable($idRequete->fetch());

        return $client;
    }

    public function addClient(ClientTable $valeurs) {
        // Requête de type Insert (création)
        $sql = "INSERT INTO client (nomPrenomClient, adresseClient, codePostalClient, villeClient, telClient, emailClient)
            VALUES (?, ?, ?, ?,?,?)";

        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getNomPrenomClient(),
            $valeurs->getAdresseClient(),
            $valeurs->getCodePostalClient(),
            $valeurs->getvilleClient(),
            $valeurs->getTelClient(),
            $valeurs->getEmailClient()
        ));

        if ($idRequete) {
            ClientTable::setMessageSucces("Création effectuée avec succès !");
        }
    }

    public function editClient(ClientTable $valeurs) {
        // Requête de type Update (Modification)
        $sql = "UPDATE client SET nomPrenomClient = ?, adresseClient = ?,codePostalClient=?, villeClient=?, telClient = ?, emailClient = ? WHERE idClient = ?";

        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getNomPrenomClient(),
            $valeurs->getAdresseClient(),
            $valeurs->getCodePostalClient(),
            $valeurs->getVilleClient(),
            $valeurs->getTelClient(),
            $valeurs->getEmailClient(),
            $valeurs->getIdClient()
        ));

        if ($idRequete) {
            ClientTable::setMessageSucces("Modification effectuée avec succès !");
        }
    }

    public function deleteClient() {

        $sql = "DELETE FROM client WHERE idClient = ?";


        $idRequete = $this->executeQuery($sql, array($this->parametre['idClient']));

        if ($idRequete) {

            ClientTable::setMessageSucces("Suppression effectuée avec succès !");
        }
    }

}
