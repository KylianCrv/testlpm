<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{$titre|upper}</title>

        <link rel="icon" type="image/png" href="ressources/public/assets/logolpm.png" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link href="ressources/public/css/style.css" rel="stylesheet">

    </head>
    <body>
        <div class="container-fluid p-0">

            <div class="row no-gutters">
                <div class="col-md-12">
                    {if $role eq 'Gestionnaire'}
                        {include file='ressources/public/menu_gestionnaire.tpl'}
                    {/if}
                    {if $role eq 'Salarié'}
                        {include file='ressources/public/menu_salarie.tpl'}
                    {/if}
                    {if $role eq 'Administrateur'}
                        {include file='ressources/public/menu_administrateur.tpl'}
                    {/if}


                    <div class="row">
                        <div class="col-md-4 p-3 space">
                            <a href="index.php"><img src="ressources/public/assets/img/logolpm.png" ></a>
                        </div>
                        <div class="col-md-6 p-3 space">
                            <h1>{$titreGestion}</h1>
                            <p {if $unClient->getMessageErreur() neq ''} class="pos-messageErreur" {/if}>
                                {$unClient->getMessageErreur()}
                            </p>
                        </div>
                        <div class="col-md-2 p-3 space">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-8">
                            <form action="index.php" method="post" novalidate="" id="formulaire">

                                <input type="hidden" name="gestion" value="client">
                                <input type="hidden" name="action" value="{$action}">




                                {if $action neq 'ajouter'}
                                    <div class="form-group">
                                        Identifiant client :
                                        <input class="form-control" id="idBiere" name="idClient" type="text" value="{$unClient->getIdClient()}" readonly>
                                    </div>
                                {/if}

                                <div class="form-group">
                                    Nom et prénom du client <sup>(*)</sup> :
                                    <strong>
                                        <input class="form-control" id="nomPrenomClient" name="nomPrenomClient" type="text" value="{$unClient->getNomPrenomClient()}" {$comportement} required="required">
                                    </strong>
                                </div>

                                <div class="form-group">
                                    Adresse du client :
                                    <input class="form-control" id="adresseClient" name="adresseClient" type="text" value="{$unClient->getAdresseClient()}" {$comportement} >
                                </div>
                                <div class="form-group">
                                    Code postal :
                                    <input class="form-control" id="codePostalClient" name="codePostalClient" type="text" value="{$unClient->getcodePostalClient()}" {$comportement} >
                                </div>
                                <div class="form-group">
                                    Localité :
                                    <input class="form-control" id="villeClient" name="villeClient" type="text" value="{$unClient->getVilleClient()}" {$comportement} >
                                </div>
                                <div class="form-group">
                                    Telephone du client :
                                    <input class="form-control" id="telClient" name="telClient" type="text" value="{$unClient->getTelClient()}" {$comportement} >
                                </div>
                                <div class="form-group">
                                    Email du client :
                                    <input class="form-control" id="emailClient" name="emailClient" type="text" value="{$unClient->getEmailClient()}" {$comportement} >
                                </div>


                                <div class="form-group">

                                    <div class="col-md-11">
                                        <input type="button"  class="btn btn-danger btn-sm"
                                               onclick='location.href = "index.php?gestion=client"' value="Retour">
                                    </div>

                                </div>

                        </div>
                        <div class="col-md-2 align-self-end pb-3">

                            {if $action neq 'consulter'}
                                <div class="col-md-4">
                                    <input type="submit" class="btn btn-danger btn-sm" value="{$action|capitalize}">
                                </div>
                            {/if}
                            {if $action eq 'ajouter'}

                                <a class="btn btn-danger btn-sm" id='sendMail'>Mail de confirmation</a>

                            {/if}

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        {include file='ressources/public/piedPage.tpl'}

        <!-- Bootstrap core JS-->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
        <!-- Core theme JS-->
        <script src="ressources/public/js/jqueryvalidate.js" type="text/javascript"></script>
        <script src="ressources/public/js/validateform.js" type="text/javascript"></script>
        <script src="ressources/public/js/script.js" type="text/javascript"></script>
    </body>
</html>