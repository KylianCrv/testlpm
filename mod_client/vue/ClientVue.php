<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clientVue
 *
 * @author Gaël
 */
class ClientVue {

    private $parametre; //array
    private $tpl; //objet
    private $valeurs;

    public function __construct($parametre) {

        $this->parametre = $parametre;

        $this->tpl = new Smarty();
    }

    public function chargementValeurs() {

        $this->tpl->assign('titre', 'Gestion La Petite Mousse');

        $this->tpl->assign('deconnexion', 'Déconnexion');
        $this->tpl->assign('role', $_SESSION['role']);
    }

    public function genererAffichageListe($valeurs) {

        $this->valeurs = $valeurs;

        $this->chargementValeurs();

        $this->tpl->assign('titreGestion', 'Liste des clients');

        $this->tpl->assign('message', ClientTable::getMessageSucces());

        $this->tpl->assign('listeClients', $this->valeurs);

        $this->tpl->display('mod_client/vue/clientListeVue2.tpl');
    }

    public function genererAffichageFiche($valeurs) {

        $this->valeurs = $valeurs;

        $this->chargementValeurs();

        switch ($this->parametre['action']) {

            case 'form_consulter':

                $this->tpl->assign('titreGestion', 'Consultation d\'un client');

                $this->tpl->assign('action', 'consulter');

                $this->tpl->assign('comportement', 'disabled');

                $this->tpl->assign('unClient', $this->valeurs);

                break;

            case 'form_ajouter':
            case 'ajouter':

                $this->tpl->assign('titreGestion', 'Création d\'un client');

                $this->tpl->assign('action', 'ajouter');

                $this->tpl->assign('comportement', '');

                $this->tpl->assign('unClient', $this->valeurs);

                break;

            case 'form_modifier':
            case 'modifier':

                $this->tpl->assign('titreGestion', 'Modification d\'un client');

                $this->tpl->assign('action', 'modifier');

                $this->tpl->assign('comportement', '');

                $this->tpl->assign('unClient', $this->valeurs);

                break;

            case 'form_supprimer':

                $this->tpl->assign('titreGestion', 'Suppression d\'un client');

                $this->tpl->assign('action', 'supprimer');

                $this->tpl->assign('comportement', 'disabled');

                $this->tpl->assign('unClient', $this->valeurs);

                break;
        }


        $this->tpl->display('mod_client/vue/clientFicheVue2.tpl');
    }

}
