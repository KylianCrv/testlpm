<?php

/**
 * Description of produitVue
 *
 * @author ishish
 */
class ProduitVue {

    private $parametre; //array
    private $tpl; //objet
    private $valeurs;

    public function __construct($parametre) {

        $this->parametre = $parametre;

        $this->tpl = new Smarty();
    }

    public function chargementValeurs() {

        $this->tpl->assign('titre', 'Gestion La Petite Mousse');

        $this->tpl->assign('deconnexion', 'Déconnexion');
        $this->tpl->assign('role', $_SESSION['role']);
    }

    public function genererAffichageListe($valeurs) {

        $this->valeurs = $valeurs;

        $this->chargementValeurs();

        $this->tpl->assign('titreGestion', 'Liste des bières');

        $this->tpl->assign('message', ProduitTable::getMessageSucces());

        $this->tpl->assign('listeProduits', $this->valeurs);

        $this->tpl->display('mod_produit/vue/produitListeVue2.tpl');
    }

    public function genererAffichageFiche($valeurs, $fermentations = null, $couleurs = null, $styles = null) {

        $this->valeurs = $valeurs;

        $this->chargementValeurs();

        switch ($this->parametre['action']) {

            case 'form_consulter':

                $this->tpl->assign('titreGestion', 'Consultation d\'une bière');

                $this->tpl->assign('action', 'consulter');

                $this->tpl->assign('comportement', 'disabled');

                $this->tpl->assign('uneBiere', $this->valeurs);

                break;

            case 'form_ajouter':
            case 'ajouter':

                $this->tpl->assign('titreGestion', 'Création d\'une bière');

                $this->tpl->assign('action', 'ajouter');

                $this->tpl->assign('comportement', '');

                $this->tpl->assign('uneBiere', $this->valeurs);

                $this->tpl->assign('fermentations', $fermentations);

                $this->tpl->assign('fermentationSelected', 1);

                $this->tpl->assign('couleurs', $couleurs);

//                $this->tpl->assign('couleurSelected', 1);

                $this->tpl->assign('styles', $styles);

                $this->tpl->assign('styleSelected', 1);
                break;

            case 'form_modifier':
            case 'modifier':

                $this->tpl->assign('titreGestion', 'Modification d\'une bière');

                $this->tpl->assign('action', 'modifier');

                $this->tpl->assign('comportement', '');

                $this->tpl->assign('uneBiere', $this->valeurs);

                $this->tpl->assign('fermentations', $fermentations);

                $this->tpl->assign('fermentationSelected', $this->valeurs->getIdFermentationBiere());

                $this->tpl->assign('couleurs', $couleurs);

                $this->tpl->assign('couleurSelected', $this->valeurs->getIdCouleurBiere());

                $this->tpl->assign('styles', $styles);

                $this->tpl->assign('styleSelected', $this->valeurs->getIdStyleBiere());
                $this->tpl->assign('fermentationType', $this->valeurs->getCommentaireTypeFermentationBiere());

                break;

            case 'form_supprimer':

                $this->tpl->assign('titreGestion', 'Suppression d\'une bière');

                $this->tpl->assign('action', 'supprimer');

                $this->tpl->assign('comportement', 'disabled');

                $this->tpl->assign('uneBiere', $this->valeurs);

                break;
        }


        $this->tpl->display('mod_produit/vue/produitFicheVue2.tpl');
    }

}
