<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{$titre|upper}</title>

        <link rel="icon" type="image/png" href="ressources/public/assets/logolpm.png" />

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link href="ressources/public/css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid p-0">

            <div class="row no-gutters">
                <div class="col-md-12">
                    {if $role eq 'Gestionnaire'}
                        {include file='ressources/public/menu_gestionnaire.tpl'}
                    {/if}
                    {if $role eq 'Salarié'}
                        {include file='ressources/public/menu_salarie.tpl'}
                    {/if}
                    {if $role eq 'Administrateur'}
                        {include file='ressources/public/menu_administrateur.tpl'}
                    {/if}

                    <div class="row">
                        <div class="col-md-4 p-3 space">
                            <a href="index.php"><img src="ressources/public/assets/img/logolpm.png" ></a>
                        </div>
                        <div class="col-md-6 p-3 space">
                            <h1>{$titreGestion}</h1>
                            <p {if $uneBiere->getMessageErreur() neq ''} class="pos-messageErreur" {/if}>
                                {$uneBiere->getMessageErreur()}
                            </p>
                        </div>
                        <div class="col-md-2 p-3 space">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-8">
                            <div class="col-md-offset-2 col-10 col-md-8 col-md-offset-2 space">
                                <form action="index.php" method="post" novalidate="">

                                    <input type="hidden" name="gestion" value="produit">
                                    <input type="hidden" name="action" value="{$action}">




                                    {if $action neq 'ajouter'}
                                        <div class="form-group">
                                            Identifiant bière :
                                            <input class="form-control" id="idBiere" name="idBiere" type="text" value="{$uneBiere->getIdBiere()}" readonly>
                                        </div>
                                    {/if}

                                    <div class="form-group">
                                        Nom de la bière <sup>(*)</sup> :
                                        <strong>
                                            <input class="form-control" id="designationBiere" name="designationBiere" type="text" value="{$uneBiere->getDesignationBiere()}" {$comportement} required="required">
                                        </strong>
                                    </div>

                                    <div class="form-group">
                                        Taux d'alcool de la bière :
                                        <input class="form-control" id="degresAlcoolBiere" name="degresAlcoolBiere" type="text" value="{$uneBiere->getDegresAlcoolBiere()}" {$comportement} >
                                    </div>

                                    {if $action eq 'consulter' or $action eq 'supprimer'}
                                        <div class="row align-items-center">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    Type de fermentation de la bière :

                                                    <input class="form-control" id="nomFermentationBiere" name="nomFermentationBiere" type="text" value="{$uneBiere->getNomFermentationBiere()}" {$comportement}>
                                                </div></div>
                                            <div class="col-md-6"><!-- Button trigger modal -->
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                                                    + de détails
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLongTitle">Info type de fermentation</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" name='commentaireTypeFermentationBiere' id="commentaireTypeFermentationBiere"><strong>Fermentation Haute :</strong> pratiquée depuis la nuit des temps, elle résulte de l’utilisation de levure comparable à celle utilisée en boulangerie. Elle se déroule à 20°C et se récolte à l’issue de la fermentation par écumage. Elle donnera des ale ou des stouts.<br>
                                                                <strong>Fermentation basse :</strong> pratiquée depuis la nuit des temps, elle résulte de l’utilisation de levure comparable à celle utilisée en boulangerie. Elle se déroule à 20°C et se récolte à l’issue de la fermentation par écumage. Elle donnera des ale ou des stouts.<br>
                                                                <strong>Fermentation spontanée :</strong> les micro-organismes sont omniprésents dans la terre, l’eau, l’air. Les levures sont véhiculées par l’air et certaines souches se développent plus particulièrement à la saison des fruits. Dans une petite région proche de Bruxelles, la vallée de la Senne et le Pajottenland, un moût de bière est laissé à refroidir à l’air libre, des microbes bienveillants vont ensemencer ce milieu nutritif. Ils donneront des produits de caractère, de grande qualité, les lambics, gueuzes et autres faros.<br>
                                                                <strong>Fermentation mixte :</strong> les micro-organismes sont omniprésents dans la terre, l’eau, l’air. Les levures sont véhiculées par l’air et certaines souches se développent plus particulièrement à la saison des fruits. Dans une petite région proche de Bruxelles, la vallée de la Senne et le Pajottenland, un moût de bière est laissé à refroidir à l’air libre, des microbes bienveillants vont ensemencer ce milieu nutritif. Ils donneront des produits de caractère, de grande qualité, les lambics, gueuzes et autres faros.

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div></div></div>
                                            {else}
                                        <div class="dropdown">
                                            <button class='btn btn-secondary dropdown-toggle my-2 w-100' type="button" id="dropdownMenuButton" data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                Type de fermentation de la bière :
                                            </button>
                                            <select class="dropdown-menu w-100" name="idFermentationBiere" size="{$fermentations|@count}">
                                                {html_options options=$fermentations selected=$fermentationSelected}
                                            </select>
                                        </div>
                                    {/if}
                                    {if $action eq 'consulter' or $action eq 'supprimer'}
                                        <div class="row align-items-center">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    Style de la bière :

                                                    <input class="form-control" id="nomStyleBiere" name="nomStyleBiere" type="text" value="{$uneBiere->getNomStyleBiere()}" {$comportement} >
                                                    <!-- Button trigger modal -->
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter2">
                                                    + de détails
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLongTitle">Info styles de bière</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body"><strong>Bière de garde :</strong>ce style typique du Nord de la France indique que la bière a été « gardée », autrefois en fûts, aujourd’hui en cuves avant d’être embouteillée, ce qui permet de parfaire ses arômes et son goût. Ces bières de garde peuvent être consommées plusieurs mois, voire années après la mise en bouteille.  La plupart sont assez douces, avec des notes céréalières et de caramel pour les plus foncées. Elles peuvent être de fermentation haute ou basse<br>
                                                                <strong> Bière d’abbaye :</strong>  le plus souvent blonde ou ambrée, sa recette doit s’appuyer sur une production historique de bière dans une abbaye, les moines étant autrefois des brasseurs notoires. La bière d’abbaye est ronde en bouche, peu amère et riche en malt<br>
                                                                <strong>Lager :</strong>c’est la bière blonde classique, de fermentation basse, légère, titrant de 4 à 5%vol. Elle développe le plus souvent des notes florales et une amertume légère. Sa déclinaison la plus éminente est la Pils* aux saveurs fines de malt et de houblon. Elle représente 90% de la production mondiale.<br>
                                                                <strong>Pale Ale :</strong> signifie en anglais « bière pâle ». Blonde également, elle est obtenue par fermentation haute, et est élaborée à base de malts spéciaux, d’où ses notes de biscuit.<br>
                                                                <strong>India Pale Ale : </strong> dans son utilisation actuelle, cette ale se distingue par une forte amertume et des arômes intenses d’agrumes. Redécouverte par les brasseurs artisanaux américains dans les années 80, la recette initiale a été mise au point par les Britanniques au XIXème siècle pour permettre à leurs bières de mieux voyager par bateau jusqu’aux Indes, en augmentant la quantité de houblon dans leur brassage afin d’améliorer la conservation de la bière jusqu’à destination.<br>
                                                                <strong>Triple :</strong> Bière plus aromatique et ronde en bouche, de fermentation haute, elle est produite avec des levures particulières qui lui confèrent des saveurs de fruits mûrs, ainsi que des notes épicées de clou de girofle.<br>
                                                                <strong>Stout :</strong> cette ale de tradition anglaise se distingue des autres bières brunes par un goût de grillé persistant, grâce à des malts très torréfiés au goût de café, voire de cacao. Sa couleur foncée n’a pas d’incidence sur le degré alcoolique, puisque la plupart titre entre 4 et 6%vol.<br>
                                                                <strong>Blanche :</strong> une forte proportion de blé apporte acidité et fraîcheur à ces bières très effervescentes, qui ne sont pas forcément blanches, mais souvent troubles car non filtrées. Les styles allemands comme la Wiezenbier présentent un léger parfum de banane. Les Witbier, de tradition belge, plus courantes en France, sont agrémentées de graines de coriandre et d’écorces d’agrumes.<br>
                                                                <strong>Saison :</strong> encore confidentielle, la Saison est une bière originaire des fermes brassicoles du nord de la France et de Belgique. Fraîche et fruitée, elle est brassée avec une levure particulière et des houblons belges ou anglais qui lui confèrent des saveurs herbacées.<br>
                                                                <strong>Sour :</strong> acide en anglais, sont des bières de tradition belge dont l’acidité est dûe à l’action des lactobacilles dont la maîtrise relève d’un vrai savoir-faire. Le moût souvent laissé à l’air libre est ensemencé par les organismes présents dans l’air et se prête ensuite à différentes recettes : rajout de fruits, vieillissement en tonneaux pour donner naissance à la lambic ou la gueuze. On trouve de plus en plus de bières sour produites par des brasseurs français.<br>
                                                                <strong>Vieillie en fût :</strong> certaines bières se prêtent à des vieillissements en fûts de chêne ayant contenu des vins ou des liqueurs. Pendant une durée de 3 à 18 mois, la bière s’approprie les arômes du bois mais aussi ceux laissées par les boissons vieillies auparavant. Ces bières de dégustation sont prisées par la restauration pour la richesse de leurs saveurs.<br>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    {else}
                                        <div class="dropdown">
                                            <button class='btn btn-secondary dropdown-toggle my-2 w-100' type="button" id="dropdownMenuButton" data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                Style de la bière :
                                            </button>
                                            <select class="dropdown-menu w-100" name="idStyleBiere" size="{$styles|@count}">
                                                {html_options options=$styles selected=$styleSelected}
                                            </select>
                                        </div>
                                    {/if}
                                    {if $action eq 'consulter' or $action eq 'supprimer'}
                                        <div class="row align-items-center">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    Couleur de la bière :

                                                    <input class="form-control" id="nomCouleurBiere" name="nomCouleurBiere" type="text" value="{$uneBiere->getNomCouleurBiere()}" {$comportement} >
                                                </div></div>
                                            <div class="col-md-6"<!-- Button trigger modal -->
                                                 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter3">
                                                    + de détails
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="exampleModalCenter3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLongTitle">Info couleurs de bière</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body"><strong>Blanche :</strong> brassées avec des malts pâles et des malts de froment. Le malt de blé ne comporte pas d’enveloppes comme le malt d’orge, aussi les matières colorantes sont moindres<br>
                                                                <strong>Blonde :</strong> brassées avec des malts d’orge pâles.<br>
                                                                <strong>Ambrée :</strong> brassées avec des malts dits « caramel » plus grillés et torréfiés que les bières blondes. <br>
                                                                <strong>Rousse :</strong> brassées avec des malts dits « caramel » plus grillés et torréfiés que les bières blondes. <br>
                                                                <strong>Brune : </strong>brassées avec des malts bruns, appelés malts « chocolat » fortement torréfiés qui leur confèrent un fort potentiel aromatique et des couleurs allant du marron au noir.<br>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div></div></div>
                                            {else}
                                        <div class="dropdown ">
                                            <button class='btn btn-secondary dropdown-toggle my-2 w-100' type="button" id="dropdownMenuButton" data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                Couleur de la bière :
                                            </button>
                                            <select class="dropdown-menu w-100" name="idCouleurBiere" size="{$couleurs|@count}">
                                                {html_options options=$couleurs selected=$couleurSelected}
                                            </select>
                                        </div>
                                    {/if}

                                    <div class="form-group">

                                        <div class="col-md-8">
                                            <input type="button"  class="btn btn-danger btn-sm"
                                                   onclick='location.href = "index.php?gestion=produit"' value="Retour">
                                        </div>

                                    </div>


                            </div>

                        </div>
                        <div class="col-md-2 align-self-end pb-3">

                            {if $action neq 'consulter'}

                                <input type="submit" class="btn btn-danger btn-sm" value="{$action|capitalize}">

                            {/if}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {include file = 'ressources/public/piedPage.tpl'}
            <!-- Bootstrap core JS-->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
            <!-- Third party plugin JS-->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
            <!-- Core theme JS-->
            <script src="ressources/public/js/script.js"></script>

    </body>
</html>