<?php

/**
 * Description of produitModele
 *
 * @author ishish
 */
class ProduitModele extends Modele {

    private $parametre;

    public function __construct($parametre) {

        $this->parametre = $parametre;
    }

    public function getListeProduits() {

        //Requête attendue de type SELECT (liste des lieux)
        $sql = "SELECT * FROM biere
JOIN couleur_biere ON biere.idCouleurBiere = couleur_biere.idCouleurBiere";


        $idRequete = $this->executeQuery($sql);

        return $idRequete->fetchall(PDO::FETCH_ASSOC);
    }

    public function getUnProduit() {

        //Requête attendue de type SELECT (un seul lieu)
        $sql = 'SELECT * FROM biere
JOIN couleur_biere ON biere.idCouleurBiere = couleur_biere.idCouleurBiere
JOIN type_fermentation_biere on biere.idFermentationBiere = type_fermentation_biere.idFermentationBiere
JOIN style_biere on biere.idStyleBiere = style_biere.idStyleBiere
WHERE idBiere = ?';

        $idRequete = $this->executeQuery($sql, array($this->parametre['idBiere']));

        //     var_dump($idRequete->fetch());
        $produit = new ProduitTable($idRequete->fetch());

        return $produit;
    }

    public function addProduit(ProduitTable $valeurs) {
        // Requête de type Insert (création)
        $sql = "INSERT INTO biere (designationBiere, degresAlcoolBiere, idFermentationBiere, idStyleBiere, idCouleurBiere)
            VALUES (?, ?,?,?,?)";

        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getDesignationBiere(),
            $valeurs->getDegresAlcoolBiere(),
            $valeurs->getIdFermentationBiere(),
            $valeurs->getIdStyleBiere(),
            $valeurs->getIdCouleurBiere()
        ));

        if ($idRequete) {
            ProduitTable::setMessageSucces("Création effectuée avec succès !");
        }
    }

    public function editProduit(ProduitTable $valeurs) {
        // Requête de type Insert (création)
        $sql = "UPDATE biere SET designationBiere = ?, degresAlcoolBiere = ?, "
                . "idFermentationBiere = ?,"
                . "idStyleBiere = ?,"
                . "idCouleurBiere = ?"
                . "WHERE idBiere = ?";


        $idRequete = $this->executeQuery($sql, array(
            $valeurs->getDesignationBiere(),
            $valeurs->getDegresAlcoolBiere(),
            $valeurs->getIdFermentationBiere(),
            $valeurs->getIdStyleBiere(),
            $valeurs->getIdCouleurBiere(),
            $valeurs->getIdBiere()
        ));

        if ($idRequete) {
            ProduitTable::setMessageSucces("Modification effectuée avec succès !");
        }
    }

    public function deleteProduit() {

        $sql = "DELETE FROM biere WHERE idBiere = ?";


        $idRequete = $this->executeQuery($sql, array($this->parametre['idBiere']));

        if ($idRequete) {

            ProduitTable::setMessageSucces("Suppression effectuée avec succès !");
        }
    }

    public function getAllFermentations() {

        $sql = "SELECT * FROM type_fermentation_biere";

        $idRequete = $this->executeQuery($sql);

        return $idRequete->fetchall(PDO::FETCH_ASSOC);
    }

    public function getAllCouleurs() {

        $sql = "SELECT * FROM couleur_biere";

        $idRequete = $this->executeQuery($sql);

        return $idRequete->fetchall(PDO::FETCH_ASSOC);
    }

    public function getAllStyles() {

        $sql = "SELECT * FROM style_biere";

        $idRequete = $this->executeQuery($sql);

        return $idRequete->fetchall(PDO::FETCH_ASSOC);
    }

}
