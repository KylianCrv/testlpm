<?php

/**
 * Description of produitControleur
 *
 * @author ishish
 */
class ProduitControleur {

    private $parametre; //array
    private $oModele; // objet
    private $oVue; // objet

    public function __construct($parametre) {

        $this->parametre = $parametre;
//Création d'un objet modele
        $this->oModele = new ProduitModele($this->parametre);
//Création d'un objet vue
        $this->oVue = new ProduitVue($this->parametre);
    }

    public function liste() {

        $valeurs = $this->oModele->getListeProduits();

        $this->oVue->genererAffichageListe($valeurs);
    }

    public function form_consulter() {

        $valeurs = $this->oModele->getUnProduit();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function form_ajouter() {

        $prepareProduit = new ProduitTable();
        $fermentations = $this->getFermentationForSelect($this->oModele->getAllFermentations());
        $couleurs = $this->getCouleurForSelect($this->oModele->getAllCouleurs());
        $styles = $this->getStyleForSelect($this->oModele->getAllStyles());
        $this->oVue->genererAffichageFiche($prepareProduit, $fermentations, $couleurs, $styles);
    }

    public function form_modifier() {

        $valeurs = $this->oModele->getUnProduit();
        $fermentations = $this->getFermentationForSelect($this->oModele->getAllFermentations());
        $couleurs = $this->getCouleurForSelect($this->oModele->getAllCouleurs());
        $styles = $this->getStyleForSelect($this->oModele->getAllStyles());
        $this->oVue->genererAffichageFiche($valeurs, $fermentations, $couleurs, $styles);
    }

    public function form_supprimer() {

        $valeurs = $this->oModele->getUnProduit();

        $this->oVue->genererAffichageFiche($valeurs);
    }

    public function ajouter() {

        $controleProduit = new ProduitTable($this->parametre);

        if ($controleProduit->getAutorisationBD() == false) {
// ici nous sommes en erreur

            $fermentations = $this->getFermentationForSelect($this->oModele->getAllFermentations());
            $couleurs = $this->getCouleurForSelect($this->oModele->getAllCouleurs());
            $styles = $this->getStyleForSelect($this->oModele->getAllStyles());
            $this->oVue->genererAffichageFiche($controleProduit, $fermentations, $couleurs, $styles);
        } else {
// ici l'insertion est possible !
            $this->oModele->addProduit($controleProduit);
//Ici l'objet controleur (oControleur)
//Il a été créé dans le routeur
            $this->liste();
        }
    }

    public function modifier() {

        $controleProduit = new ProduitTable($this->parametre);

        if ($controleProduit->getAutorisationBD() == false) {
// ici nous sommes en erreur
            $fermentations = $this->getFermentationForSelect($this->oModele->getAllFermentations());
            $couleurs = $this->getCouleurForSelect($this->oModele->getAllCouleurs());
            $styles = $this->getStyleForSelect($this->oModele->getAllStyles());
            $this->oVue->genererAffichageFiche($controleProduit, $fermentations, $couleurs, $styles);
        } else {
// ici l'édition est possible !
            $this->oModele->editProduit($controleProduit);
//Ici l'objet controleur (oControleur)
//Il a été créé dans le routeur

            $this->liste();
        }
    }

    public function supprimer() {

        $this->oModele->deleteProduit();

        $this->liste();
    }

    private function getFermentationForSelect($fermentations) {
        $tab = array();

        for ($i = 0; $i < count($fermentations); $i++) {

            $fermentation = $fermentations[$i];
            $tab[$fermentation['idFermentationBiere']] = $fermentation['nomFermentationBiere'];
        }
        return $tab;
    }

    private function getCouleurForSelect($couleurs) {
        $tab = array();

        for ($i = 0; $i < count($couleurs); $i++) {

            $couleur = $couleurs[$i];
            $tab[$couleur['idCouleurBiere']] = $couleur['nomCouleurBiere'];
        }
        return $tab;
    }

    private function getStyleForSelect($styles) {
        $tab = array();

        for ($i = 0; $i < count($styles); $i++) {

            $style = $styles[$i];
            $tab[$style['idStyleBiere']] = $style['nomStyleBiere'];
        }
        return $tab;
    }

}
