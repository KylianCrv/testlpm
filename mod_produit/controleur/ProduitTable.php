<?php

/**
 * Description of produitTable
 *
 * @author ishish
 */
class ProduitTable {

    private $idBiere = "";
    private $designationBiere = "";
    private $degresAlcoolBiere = "";
    private $idFermentationBiere = "";
    private $nomFermentationBiere = "";
    private $idStyleBiere = "";
    private $nomStyleBiere = "";
    private $idCouleurBiere = "";
    private $nomCouleurBiere = "";
    private $commentaireTypeFermentationBiere = "";
    private $autorisationBD = true;
    private static $messageErreur = "";
    private static $messageSucces = "";

    // 2 Importer la méthode hydrater
    public function hydrater(array $row) {

        foreach ($row as $k => $v) {
            // Concaténation : nom de la méthode setter à appeler
            $setter = 'set' . ucfirst($k);
            // fonction prend 2 paramètres : l'objet en cours et le nom de la méthode
            if (method_exists($this, $setter)) {
                // Invoquer la méthode
                $this->$setter($v);
            }
        }
    }

    public function __construct($data = null) {

        if ($data != null) {

            $this->hydrater($data);
        }
    }

///////////////////////////GETTER/////////////////


    function getNomFermentationBiere() {
        return $this->nomFermentationBiere;
    }

    function getNomStyleBiere() {
        return $this->nomStyleBiere;
    }

    function getNomCouleurBiere() {
        return $this->nomCouleurBiere;
    }

    function getIdBiere() {
        return $this->idBiere;
    }

    function getDesignationBiere() {
        return $this->designationBiere;
    }

    function getDegresAlcoolBiere() {
        return $this->degresAlcoolBiere;
    }

    function getIdFermentationBiere() {
        return $this->idFermentationBiere;
    }

    function getIdStyleBiere() {
        return $this->idStyleBiere;
    }

    function getIdCouleurBiere() {
        return $this->idCouleurBiere;
    }

    function getCommentaireTypeFermentationBiere() {
        return $this->commentaireTypeFermentationBiere;
    }

//SETTER/////////////////////////

    function setNomCouleurBiere($nomCouleurBiere) {
        if (ctype_space($nomCouleurBiere) || empty($nomCouleurBiere)) {
            self::setMessageErreur("La couleur de la bière est incorrecte");
            $this->setAutorisationBD(false);
        }
        $this->nomCouleurBiere = $nomCouleurBiere;
    }

    function setDesignationBiere($designationBiere) {
        if (ctype_space($designationBiere) || empty($designationBiere)) {
            self::setMessageErreur("Le nom de la bière est incorrect");
            $this->setAutorisationBD(false);
        }
        $this->designationBiere = $designationBiere;
    }

    function setDegresAlcoolBiere($degresAlcoolBiere) {
        if (ctype_space($degresAlcoolBiere) || empty($degresAlcoolBiere)) {
            self::setMessageErreur("Le degrés d'alcool est incorrect");
            $this->setAutorisationBD(false);
        }
        $this->degresAlcoolBiere = $degresAlcoolBiere;
    }

    function setIdFermentationBiere($idFermentationBiere) {
        if (ctype_space($idFermentationBiere) || empty($idFermentationBiere)) {
            self::setMessageErreur("Le type de fermentation est incorrect");
            $this->setAutorisationBD(false);
        }
        $this->idFermentationBiere = $idFermentationBiere;
    }

    function setIdStyleBiere($idStyleBiere) {
        $this->idStyleBiere = $idStyleBiere;
    }

    function setIdCouleurBiere($idCouleurBiere) {
        if (ctype_space($idCouleurBiere) || empty($idCouleurBiere)) {
            self::setMessageErreur("La couleur de la bière ne peut pas être vide");
            $this->setAutorisationBD(false);
        }
        $this->idCouleurBiere = $idCouleurBiere;
    }

    function setIdBiere($idBiere) {
        $this->idBiere = $idBiere;
    }

    function setNomFermentationBiere($nomFermentationBiere) {
        if (ctype_space($nomFermentationBiere) || empty($nomFermentationBiere)) {
            self::setMessageErreur("Le type de fermentation est incorrect");
            $this->setAutorisationBD(false);
        }
        $this->nomFermentationBiere = $nomFermentationBiere;
    }

    function setNomStyleBiere($nomStyleBiere) {
        if (ctype_space($nomStyleBiere) || empty($nomStyleBiere)) {
            self::setMessageErreur("Le style de bière est incorrect");
            $this->setAutorisationBD(false);
        }
        $this->nomStyleBiere = $nomStyleBiere;
    }

    function setCommentaireTypeFermentationBiere($commentaireTypeFermentationBiere) {
        $this->commentaireTypeFermentationBiere = $commentaireTypeFermentationBiere;
    }

////////////////////MESSAGE ERREUR/////////////////


    static function getMessageErreur() {
        return self::$messageErreur;
    }

    static function getMessageSucces() {
        return self::$messageSucces;
    }

    static function setMessageErreur($msg) {
        self::$messageErreur = self::$messageErreur . $msg . "<br>";
    }

    static function setMessageSucces($msg) {
        self::$messageSucces = self::$messageSucces . $msg . "<br>";
    }

    function setAutorisationBD($autorisationBD) {
        $this->autorisationBD = $autorisationBD;
    }

    function getAutorisationBD() {
        return $this->autorisationBD;
    }

}
